inherit autotools

DESCRIPTION = "Library and routing applications for diagnostic traffic"
HOMEPAGE = "http://support.cdmatech.com"
LICENSE = "QUALCOMM Proprietary"

PR = "r2"

SRC_URI = "file://${WORKSPACE}/diag"

S = "${WORKDIR}/diag"

EXTRA_OECONF += "--with-glib"
