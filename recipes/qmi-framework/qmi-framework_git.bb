DESCRIPTION = "QMI Framework Library"
LICENSE = "QUALCOMM Proprietary"
PR = "r1"

SRC_URI = "file://${WORKSPACE}/qmi-framework"

inherit autotools

S = "${WORKDIR}/qmi-framework"
