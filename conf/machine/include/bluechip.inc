# use this for overrides
MACHINE_CLASS = "bluechip"

# CPU
BASE_PACKAGE_ARCH = "armv7"
FEED_ARCH = "armv7"
TARGET_ARCH = "arm"
TARGET_CC_ARCH = "-march=armv7-a -mfpu=neon -mfloat-abi=softfp"
